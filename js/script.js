const btn = document.getElementById('main-btn');
const tableWrapper = document.querySelector('.table-wrapper');
const table = document.createElement('table');

const clearTable = () => {
    let tableSelector = document.querySelector(".main-table");
    if (tableSelector !== null) {
        while (tableSelector.hasChildNodes()) {
            tableSelector.removeChild(tableSelector.lastChild);
        }
    }
}
const getRandomColor = () => {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}


btn.addEventListener('click', (e) => {
    clearTable();
    table.className = 'main-table';
    const row = document.getElementById('input-row');
    const column = document.getElementById('input-column');
    for (let i = 0; i < row.value; i++) {
        let tr = document.createElement('tr');
        for (let j = 0; j < column.value; j++) {
            let td = document.createElement('td');

            td.className = 'table-ceil';
            td.addEventListener('click', () => {
                td.style.backgroundColor = (td.style.backgroundColor ? '' : getRandomColor());
            });
            td.innerHTML = `${i + 1}${j + 1}`;
            tr.appendChild(td);
        }
        table.appendChild(tr);
    }
    tableWrapper.appendChild(table);
});


